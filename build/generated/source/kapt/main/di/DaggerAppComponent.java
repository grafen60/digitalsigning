package di;

import dagger.internal.Preconditions;
import di.modules.AppModule;
import javax.annotation.processing.Generated;
import models.ca.CertificationAuthorityImpl;
import models.managers.FileManager;
import models.managers.KeyGenerator;
import models.managers.RSA;
import models.user.UserImpl;
import ui.MainController;
import ui.MainController_MembersInjector;
import ui.PresenterImpl;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DaggerAppComponent implements AppComponent {
  private DaggerAppComponent() {

  }

  public static Builder builder() {
    return new Builder();
  }

  public static AppComponent create() {
    return new Builder().build();
  }

  private CertificationAuthorityImpl getCertificationAuthorityImpl() {
    return new CertificationAuthorityImpl(new KeyGenerator(), new RSA());}

  private UserImpl getUserImpl() {
    return new UserImpl(new FileManager(), new RSA());}

  private PresenterImpl getPresenterImpl() {
    return new PresenterImpl(getCertificationAuthorityImpl(), getUserImpl(), getUserImpl());}

  @Override
  public void inject(MainController mainController) {
    injectMainController(mainController);}

  private MainController injectMainController(MainController instance) {
    MainController_MembersInjector.injectPresenter(instance, getPresenterImpl());
    return instance;
  }

  public static final class Builder {
    private Builder() {
    }

    /**
     * @deprecated This module is declared, but an instance is not used in the component. This method is a no-op. For more, see https://dagger.dev/unused-modules.
     */
    @Deprecated
    public Builder appModule(AppModule appModule) {
      Preconditions.checkNotNull(appModule);
      return this;
    }

    public AppComponent build() {
      return new DaggerAppComponent();
    }
  }
}
