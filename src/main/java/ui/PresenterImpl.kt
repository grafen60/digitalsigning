package ui

import models.ca.CertificationAuthority
import models.ca.certificate.Subject
import models.user.User
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import utils.IdentityType
import utils.LogEvent
import javax.inject.Inject

/**
 * Presenter represents logic layer of application. It controls UI layer through MainContract.View interface.
 * Presenter holds instances of users and certification authority. All requests from UI layer come through presenter, which decides
 * how to act on them.
 *
 * Application output is delivered via eventBus, which is subscribed by function onMessageEvent. These messages for application user
 * are then passed to ui layer to be displayed.
 */
class PresenterImpl @Inject constructor(
        private val certificationAuthority: CertificationAuthority,
        private val user1: User,
        private val user2: User
) : MainContract.Presenter {
    private lateinit var view: MainContract.View

    /**
     * After presenter is attached to the view, it registers subscription to EventBus and initialize all major components - user1 user2 and certification authority
     */
    override fun attach(view: MainContract.View) {
        this.view = view
        EventBus.getDefault().register(this)
        certificationAuthority.init()
        user1.initUser(IdentityType.USER_1, certificationAuthority)
        user2.initUser(IdentityType.USER_2, certificationAuthority)
    }

    /**
     * When user request certificate for user1 or user2, certification authority created certificate, which is then passed
     * to specific user in form of certificateData - Pair of Certificate and private key
     */
    override fun userAskForCertificate(user: IdentityType) {
        when (user) {
            IdentityType.USER_1 -> {
                val certificateData = certificationAuthority.issueCertificate(Subject.user1())
                user1.setCertificateData(certificateData)
            }
            IdentityType.USER_2 -> {
                val certificateData = certificationAuthority.issueCertificate(Subject.user1())
                user2.setCertificateData(certificateData)
            }
            else -> {
            }
        }
    }

    /**
     * Request to revoke certificate is passed to user object which handles it
     */
    override fun userRevokesCertificate(user: IdentityType) {
        when (user) {
            IdentityType.USER_1 -> user1.revokeCertificate()
            IdentityType.USER_2 -> user2.revokeCertificate()
            else -> {
            }
        }
    }

    /**
     * Request to send signed document is passed to user object which handles it
     */
    override fun sendSignedDocument(documentPath: String, sender: IdentityType, canalFolderPath: String) {
        when (sender) {
            IdentityType.USER_1 -> user1.createAndSendSignedDocument(documentPath, canalFolderPath)
            IdentityType.USER_2 -> user2.createAndSendSignedDocument(documentPath, canalFolderPath)
            else -> {
            }
        }
    }

    /**
     * Request to receive and verify signed document is passed to user object which handles it
     */
    override fun receiveAndVerifyDocumentFromCanal(canalFolderPath: String, receiver: IdentityType) {
        when (receiver) {
            IdentityType.USER_1 -> user1.receiveAndVerifyDocument(canalFolderPath)
            IdentityType.USER_2 -> user2.receiveAndVerifyDocument(canalFolderPath)
            else -> {
            }
        }
    }

    /**
     * Subscription to eventBus for log messages from components (User1, User2, CertificationAuthority)
     */
    @Subscribe
    fun onMessageEvent(event: LogEvent) {
        when (event.identityType) {
            IdentityType.USER_1 -> view.displayUser1Log(event.message)
            IdentityType.USER_2 -> view.displayUser2Log(event.message)
            IdentityType.CERTIFICATION_AUTHORITY -> view.displayCaLog(event.message)
        }
    }
}