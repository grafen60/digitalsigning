package ui;

import di.ComponentUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.jetbrains.annotations.NotNull;
import utils.IdentityType;

import javax.inject.Inject;
import java.io.File;
import java.net.URL;
import java.sql.Date;
import java.time.Instant;
import java.util.ResourceBundle;

/**
 * MainController is representing UI layer of application. It captures user's input and displays program output.
 * <p>
 * All user's input is then passed to presenter which represent logic layer of application.
 * Communication with presenter is through MainContract interface, which describes communication between UI and lagic layer
 * <p>
 * Application's output is displayed as logs for every major component
 */
public class MainController implements MainContract.View, Initializable {

    //textAres as logs of main app component - user1, user2 and certification authority
    @FXML
    TextArea taCaLog;
    @FXML
    TextArea taUser1Log;
    @FXML
    TextArea taUser2Log;

    private final FileChooser fileChooser = new FileChooser();
    private final DirectoryChooser directoryChooser = new DirectoryChooser();
    @FXML
    TextArea taFilePath1;
    @FXML
    TextArea taCanalFolder;

    @Inject
    public MainContract.Presenter presenter;
    private Stage stage;
    @FXML
    TextArea taFilePath2;


    public void requestCertificateForUser1(ActionEvent event) {
        presenter.userAskForCertificate(IdentityType.USER_1);
    }

    public void requestCertificateForUser2(ActionEvent event) {
        presenter.userAskForCertificate(IdentityType.USER_2);
    }

    public void loadPathForUser1(ActionEvent event) {
        fileChooser.setTitle("select document");
        File file = fileChooser.showOpenDialog(stage);
        taFilePath1.setText(file.getAbsolutePath());
    }

    public void loadPathForUser2(ActionEvent event) {
        fileChooser.setTitle("select document");
        File file = fileChooser.showOpenDialog(stage);
        taFilePath1.setText(file.getAbsolutePath());
    }

    public void setPublicCanalFolder(ActionEvent event) {
        directoryChooser.setTitle("select folder for canal");
        File folder = directoryChooser.showDialog(stage);
        taCanalFolder.setText(folder.getAbsolutePath());
    }

    public void sendSignedDocumentFromUser1(ActionEvent event) {
        presenter.sendSignedDocument(taFilePath1.getText(), IdentityType.USER_1, taCanalFolder.getText());
    }

    public void sendSignedDocumentFromUser2(ActionEvent event) {
        presenter.sendSignedDocument(taFilePath1.getText(), IdentityType.USER_2, taCanalFolder.getText());
    }

    public void user1LoadAndVerifyDocument(ActionEvent event) {
        presenter.receiveAndVerifyDocumentFromCanal(taCanalFolder.getText(), IdentityType.USER_1);
    }

    public void user2LoadAndVerifyDocument(ActionEvent event) {
        presenter.receiveAndVerifyDocumentFromCanal(taCanalFolder.getText(), IdentityType.USER_2);
    }

    public void revokeUser1Certificate(ActionEvent event) {
        presenter.userRevokesCertificate(IdentityType.USER_1);
    }

    public void revokeUser2Certificate(ActionEvent event) {
        presenter.userRevokesCertificate(IdentityType.USER_2);
    }

    //*************** MainContract.View methods ***************************
    @Override
    public void displayCaLog(@NotNull String log) {
        taCaLog.setText(taCaLog.getText() + Date.from(Instant.now()) + " " + log + "\n\n");
    }

    @Override
    public void displayUser1Log(@NotNull String log) {
        taUser1Log.setText(taUser1Log.getText() + Date.from(Instant.now()) + " " + log + "\n\n");
    }

    @Override
    public void displayUser2Log(@NotNull String log) {
        taUser2Log.setText(taUser2Log.getText() + Date.from(Instant.now()) + " " + log + "\n\n");
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //injectuje proměnné označené @Inject
        ComponentUtil.appComponent.inject(this);
        presenter.attach(this);
    }
}
