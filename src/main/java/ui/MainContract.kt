package ui

import utils.IdentityType

/**
 * It defines communication between login and UI layer
 */
interface MainContract {
    /**
     * View interface - defines method accessible to Logic Layer - Presenter
     */
    interface View {
        fun displayUser1Log(log: String)
        fun displayUser2Log(log: String)
        fun displayCaLog(log: String)
    }

    /**
     * Presenter interface - defines method accessible to UI layer - View
     */
    interface Presenter {
        fun userAskForCertificate(user: IdentityType)
        fun userRevokesCertificate(user: IdentityType)
        fun sendSignedDocument(documentPath: String, sender: IdentityType, canalFolderPath: String)
        fun receiveAndVerifyDocumentFromCanal(canalFolderPath: String, receiver: IdentityType)
        fun attach(view: View)
    }
}