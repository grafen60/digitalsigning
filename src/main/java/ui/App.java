package ui;

import di.ComponentUtil;
import di.DaggerAppComponent;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class App extends Application {
    public static void main(String[] args) {
        ComponentUtil.appComponent = DaggerAppComponent.builder().build();
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        //injectuje proměnné označené @Inject
        // ComponentUtil.appComponent.inject(this);
        //this.stage = primaryStage;
        Parent root = FXMLLoader.load(getClass().getResource("scene.fxml"));
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();

//
//        presenter.attach(this);
    }
}
