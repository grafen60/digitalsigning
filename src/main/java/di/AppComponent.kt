package di

import dagger.Component
import di.modules.AppModule
import di.modules.BindModule
import ui.MainController
import javax.inject.Singleton

/**
 * Dependency Injection App component inject all dependencies into specified components and resolve dependency tree
 */

@Singleton
@Component(modules = [AppModule::class, BindModule::class])
interface AppComponent {
    fun inject(mainController: MainController)
}