package di

/**
 * Singleton for obtaining app component for dependency injection
 */
object ComponentUtil {
    lateinit var appComponent: AppComponent
}