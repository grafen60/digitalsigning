package di.modules

import dagger.Binds
import dagger.Module
import models.ca.CertificationAuthority
import models.ca.CertificationAuthorityImpl
import models.user.User
import models.user.UserImpl
import ui.MainContract
import ui.PresenterImpl

/**
 * This module specifies which concrete implementation will be injected in specified interfaces
 */
@Module
abstract class BindModule {

    @Binds
    abstract fun presenterImplBind(presenterImpl: PresenterImpl): MainContract.Presenter

    @Binds
    abstract fun certificationAuthority(certificationAuthorityImpl: CertificationAuthorityImpl): CertificationAuthority

    @Binds
    abstract fun user(userImpl: UserImpl): User
}