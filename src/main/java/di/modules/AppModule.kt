package di.modules

import dagger.Module

/**
 * This modules create dependencies which cannot be resolved automatically
 */
@Module
class AppModule