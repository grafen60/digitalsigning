package models.user

import models.ca.CertificationAuthority
import models.ca.certificate.Certificate
import models.ca.key.PrivateKey
import models.ca.key.PublicKey
import models.managers.FileManager
import models.managers.RSA
import org.greenrobot.eventbus.EventBus
import utils.IdentityType
import utils.LogEvent
import java.security.MessageDigest
import javax.inject.Inject

/**
 * User object represent user, sho can send signed documents and also who can receive and verify signed documents.
 * It represents combination od logic and data layer of the application.
 */
class UserImpl @Inject constructor(private val fileManager: FileManager, private val rsa: RSA) : User {
    private var userCertificate: Certificate? = null
    private var privateKey: PrivateKey? = null
    private lateinit var certificationAuthority: CertificationAuthority
    private lateinit var userId: IdentityType

    /**
     * Function initialize user with identification in form of IdentityType and certification authority object
     */
    override fun initUser(identity: IdentityType, certificationAuthority: CertificationAuthority) {
        userId = identity
        this.certificationAuthority = certificationAuthority
    }

    /**
     * After certification authority created certificate with privateKey, this certification data are stored in user object
     * After user receives this data, it send notification to UI via eventBus
     */
    override fun setCertificateData(certificateData: Pair<Certificate, PrivateKey>) {
        userCertificate = certificateData.first
        privateKey = certificateData.second
        EventBus.getDefault().post(LogEvent(userId, "User obtained certificate and private key"))
    }

    /**
     * It loads document in documentPath, sign it and document+signature+certificate is then saved into folder at canalFolderPath
     * which simulates public canal.
     */
    override fun createAndSendSignedDocument(documentPath: String, canalFolderPath: String) {
        //check whether user have certificate
        if (userCertificate == null || privateKey == null) {
            EventBus.getDefault().post(LogEvent(userId, "ERROR: user doesn't have a certificate"))
            return
        }
        //check whether app user set correct document path
        if (documentPath.isBlank()) {
            EventBus.getDefault().post(LogEvent(userId, "ERROR: document is not loaded"))
            return
        }
        //check whether app user set correct canal folder path
        if (canalFolderPath.isBlank()) {
            EventBus.getDefault().post(LogEvent(userId, "ERROR: set canal folder!"))
            return
        }

        //load document with fileManager object
        EventBus.getDefault().post(LogEvent(userId, "Reading document..."))
        val data = fileManager.loadDocument(documentPath) ?: let {
            EventBus.getDefault().post(LogEvent(userId, "ERROR document not found"))
            return
        }
        //create document hash
        EventBus.getDefault().post(LogEvent(userId, "Creating Hash..."))
        val hash = getHash(data)
        EventBus.getDefault().post(LogEvent(userId, "Creating Signature..."))
        //create signature with RSA object
        val signature = rsa.encrypt(hash, privateKey!!)
        //save document+signature+certificate to canal folder
        saveSignedDocumentInCanalFolder(canalFolderPath, data, signature!!, userCertificate!!, documentPath.substringAfter("."))
        //inform app user, then document was successfully signed and send to canal
        EventBus.getDefault().post(LogEvent(userId, "Document was sent."))
    }

    /**
     * This function will save document+signature+certificate into specific folder, which simulates transport canal
     * All io operations are done via fileManager object
     */
    private fun saveSignedDocumentInCanalFolder(canalFolderPath: String, data: ByteArray, signature: ByteArray, certificate: Certificate, type: String) {
        fileManager.saveDocument(canalFolderPath, data, "document.$type")
        fileManager.saveSignature(canalFolderPath, signature)
        fileManager.saveCertificate(canalFolderPath, certificate)
    }

    /**
     * Function loads document+signature+certificate from canal folder
     * after that it verify certificate and document. If all is successful or if there is some error, user is notified via eventBus
     */
    override fun receiveAndVerifyDocument(canalFolderPath: String) {
        //firstly load document+certificate+signature from canal
        val document = fileManager.loadDocumentFromCanal(canalFolderPath)
        val signature = fileManager.loadSignature("$canalFolderPath/signature.json")
        val certificate = fileManager.loadCertificate("$canalFolderPath/certificate.json")

        //check, whether all parts of signed document were loaded
        if (document == null || signature == null || certificate == null) {
            EventBus.getDefault().post(LogEvent(userId, "ERROR: Document, certificate or signature not found!"))
            return
        } else {
            EventBus.getDefault().post(LogEvent(userId, "User received signed document"))
        }

        //check whether certificate is still valid
        if (certificationAuthority.isCertificateValid(certificate.serialNumber)) {
            EventBus.getDefault().post(LogEvent(userId, "Certificate is valid"))
        } else {
            EventBus.getDefault().post(LogEvent(userId, "ERROR: Certificate is NOT valid!!!"))
            return
        }

        //verify certificate with public key of certification authority
        if (verifyCertificate(certificate, certificationAuthority.getPublicKey())) {
            EventBus.getDefault().post(LogEvent(userId, "Certificate verified"))
        } else {
            EventBus.getDefault().post(LogEvent(userId, "ERROR: Certificate is NOT Verified!"))
            return
        }

        //create own hash of document
        val hash1 = getHash(document)

        //decrypt hash from signature, while using public key from certificate
        val hash2 = rsa.decrypt(signature, certificate.publicKeyInfo.publicKey)

        //compare hashes and inform app user of result
        if (hash1.contentEquals(hash2!!)) {
            EventBus.getDefault().post(LogEvent(userId, "SUCCESS: document verified!!!"))
        } else {
            EventBus.getDefault().post(LogEvent(userId, "ERROR: Document NOT verified!!!"))
        }
    }

    /**
     * function for verification of certificate
     */
    private fun verifyCertificate(certificate: Certificate, publicKey: PublicKey): Boolean {
        //create certificate hash
        val caHash1 = certificate.getSha1Hash()
        //decrypt hash from certificate signature and certification authority public key
        val caHash2 = rsa.decrypt(certificate.certificateSignature!!, publicKey)
        //compare hashes
        return caHash1.contentEquals(caHash2!!)
    }

    /**
     * Function create SHA1 hash of data in form of byteArray
     */
    private fun getHash(data: ByteArray): ByteArray {
        val md = MessageDigest.getInstance("SHA1")
        return md.digest(data)
    }

    /**
     * Function for revoking user certificate.
     * User will revoke certificate in certification authority, and then it will clear is certificate instance by setting it to null
     */
    override fun revokeCertificate() {
        userCertificate ?: return
        certificationAuthority.revokeCertificate(userCertificate!!.serialNumber)
        userCertificate = null
        EventBus.getDefault().post(LogEvent(userId, "User revoked certificate"))
    }

}
