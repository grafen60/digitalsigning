package models.user

import models.ca.CertificationAuthority
import models.ca.certificate.Certificate
import models.ca.key.PrivateKey
import utils.IdentityType

/**
 * User interface defines communication between presenter and user object
 */
interface User {
    fun initUser(identity: IdentityType, certificationAuthority: CertificationAuthority)
    fun setCertificateData(certificateData: Pair<Certificate, PrivateKey>)
    fun createAndSendSignedDocument(documentPath: String, canalFolderPath: String)
    fun receiveAndVerifyDocument(canalFolderPath: String)
    fun revokeCertificate()
}