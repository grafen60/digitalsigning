package models.managers

import com.google.gson.Gson
import com.google.gson.stream.JsonReader
import models.ca.certificate.Certificate
import java.io.*
import java.nio.file.Files
import javax.inject.Inject

/**
 * FileManager is used for IO file operations
 */
class FileManager @Inject constructor() {

    /**
     * loads document as byteArray from folder which simulates canal
     */
    fun loadDocumentFromCanal(folderPath: String): ByteArray? {
        val folder = File(folderPath)
        folder.listFiles()!!.forEach {
            if (it.name.startsWith("document")) {
                return loadDocument(it.absolutePath)
            }
        }
        return null
    }

    //load document from specific filePath
    fun loadDocument(filePath: String): ByteArray? {
        return Files.readAllBytes(File(filePath).toPath())
    }

    //load certificate from specific filePath (canal folder)
    fun loadCertificate(filePath: String): Certificate? {
        return loadJson(File(filePath))
    }

    //load signature from specific filePath (canal folder)
    fun loadSignature(filePath: String): ByteArray? {
        return loadJson(File(filePath))
    }

    //Save document to specific filePath (canal folder)
    fun saveDocument(folderPath: String, data: ByteArray, fileName: String) {
        val out: OutputStream = FileOutputStream("$folderPath/$fileName")
        out.write(data)
        out.close()
    }

    //save certificate as json to specific filePath (canal folder)
    fun saveCertificate(canalFolderPath: String, certificate: Certificate) {
        saveJson("$canalFolderPath/certificate.json", certificate)
    }

    //save signature as json to specific filePath (canal folder)
    fun saveSignature(canalFolderPath: String, signature: ByteArray) {
        saveJson("$canalFolderPath/signature.json", signature)
    }

    //function for saving any object as json to specific filepath
    private fun <T> saveJson(path: String, data: T) {
        FileWriter(path).use { file ->
            val jsonString: String = Gson().toJson(data)
            val utf8JsonString = jsonString.toByteArray(charset("UTF8"))
            file.write(String(utf8JsonString))
        }
    }

    //function for loading json and parsing it into object T
    private inline fun <reified T> loadJson(file: File): T? {
        val fileReader = FileReader(file)
        val reader = JsonReader(fileReader)
        val obj = Gson().fromJson<T>(reader, T::class.java)
        reader.close()
        fileReader.close()
        return obj
    }
}
