package models.managers

import models.ca.key.KeyPair
import models.ca.key.PrivateKey
import models.ca.key.PublicKey
import java.math.BigInteger
import java.util.*
import javax.inject.Inject

/**
 * Class for generating RSA keyPair
 */
class KeyGenerator @Inject constructor() {

    fun generateKeyPair(): KeyPair {
        val p = BigInteger.probablePrime(1023, Random())
        val q = BigInteger.probablePrime(1026, Random())

        val n = p.times(q)
        val fi = (p - 1.toBigInteger()).times(q - 1.toBigInteger())

        var e: BigInteger
        do {
            e = BigInteger(fi.bitLength(), Random())
        } while (gcd(e, fi) != 1.toBigInteger() || e >= fi)

        val d: BigInteger = e.modInverse(fi)

        val publicKey = PublicKey(n, e)
        val privateKey = PrivateKey(n, d)
        return KeyPair(privateKey, publicKey)
    }


    private fun gcd(a: BigInteger, b: BigInteger): BigInteger {
        return if (a == 0.toBigInteger()) b else gcd(b % a, a)
    }
}