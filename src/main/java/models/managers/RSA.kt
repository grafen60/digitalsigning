package models.managers

import models.ca.key.PrivateKey
import models.ca.key.PublicKey
import java.math.BigInteger
import javax.inject.Inject

/**
 * RSA object for encrypting and decrypting data (creating signatures)
 */
class RSA @Inject constructor() {
    /**
     * mod pow cannot work with negative values so when is byteArray transformed to BigInteger it is increased by offset so it will be positive
     * after decription the offset is substracted from result
     *
     * It isn't ideal solution but it works great with byte array with length up to 256 (can be increased by bigger offset array size). hash is only 16 Bytes, so for this purpose
     */

    private val offset = BigInteger(Array(256) { return@Array Byte.MAX_VALUE }.toByteArray())

    fun encrypt(input: ByteArray, privateKey: PrivateKey): ByteArray? {
        return (BigInteger(input) + offset).modPow(privateKey.d, privateKey.modulus).toByteArray()
    }

    fun decrypt(input: ByteArray, publicKey: PublicKey): ByteArray? {
        return (BigInteger(input).modPow(publicKey.e, publicKey.modulus) - offset).toByteArray()
    }

}

