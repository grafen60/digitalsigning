package models.ca.certificate

import models.ca.key.PublicKey
import java.io.ByteArrayOutputStream
import java.io.ObjectOutputStream
import java.io.Serializable
import java.security.MessageDigest


class Certificate(
        val version: Int = 0x00,
        val serialNumber: Int,
        val signatureAlgorithm: String = "sha1withRSAEncryption",
        val issuer: Subject,
        val validity: Validity = Validity(System.currentTimeMillis(), System.currentTimeMillis() + 1000 * 3600 * 24 * 365.toLong()),
        val subject: Subject,
        val publicKeyInfo: PublicKeyInfo,
        val signatureAlgorithm_recap: String = "sha1withRSAEncryption"
) {
    var certificateSignature: ByteArray? = null

    fun getSha1Hash(): ByteArray {
        val md = MessageDigest.getInstance("SHA1")
        return md.digest(getCertificateBytes())
    }

    fun getCertificateBytes(): ByteArray {
        return byteArrayOf(
                version.toByte(),
                serialNumber.toByte()
        ).plus(signatureAlgorithm.toByteArray())
                .plus(issuer.toByteArray())
                .plus(validity.toByteArray())
                .plus(subject.toByteArray())
                .plus(publicKeyInfo.toByteArray())
    }


}

class Validity(
        val notBefore: Long,
        val notAfter: Long
) : Serializable

class PublicKeyInfo(val publicKeyAlgorithm: String = "md5withRSAEncryption", val publicKey: PublicKey) : Serializable


class Subject(
        val C: String,
        val ST: String,
        val L: String,
        val O: String,
        val OU: String,
        val CN: String
) : Serializable {

    companion object {
        fun caAuthority() = Subject("CZ",
                "JM",
                "Brno",
                "VUT",
                "FEKT",
                "MRKI_CERTIFICATION_AUTHORITY")

        fun user1() = Subject("CZ",
                "JM",
                "Brno",
                "VUT",
                "FEKT",
                "MRKI_USER_1")

        fun user2() = Subject("CZ",
                "JM",
                "Brno",
                "VUT",
                "FEKT",
                "MRKI_USER_2")
    }
}

fun Any.toByteArray(): ByteArray {
    val bos = ByteArrayOutputStream()
    val oos = ObjectOutputStream(bos)
    oos.writeObject(this)
    oos.flush()
    return bos.toByteArray()
}