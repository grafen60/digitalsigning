package models.ca

import models.ca.certificate.Certificate
import models.ca.certificate.Subject
import models.ca.key.PrivateKey
import models.ca.key.PublicKey

/**
 * Certification authority interface defines communication between presenter and certification authority object
 */
interface CertificationAuthority {
    fun init()
    fun issueCertificate(userCredentials: Subject): Pair<Certificate, PrivateKey>
    fun revokeCertificate(certificateSerialNumber: Int)
    fun isCertificateValid(serialNumber: Int): Boolean
    fun getPublicKey(): PublicKey
    fun getActiveCertificates(): ArrayList<Int>
    fun getRevokeCertificates(): ArrayList<Int>
}
