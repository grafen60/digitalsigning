package models.ca.key

import java.io.Serializable
import java.math.BigInteger

data class PublicKey(val modulus: BigInteger, val e: BigInteger) : Serializable