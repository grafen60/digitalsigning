package models.ca.key

data class KeyPair(val privateKey: PrivateKey, val publicKey: PublicKey)