package models.ca.key

import java.math.BigInteger

data class PrivateKey(val modulus: BigInteger, val d: BigInteger)