package models.ca

import models.ca.certificate.Certificate
import models.ca.certificate.PublicKeyInfo
import models.ca.certificate.Subject
import models.ca.key.KeyPair
import models.ca.key.PrivateKey
import models.ca.key.PublicKey
import models.managers.KeyGenerator
import models.managers.RSA
import org.greenrobot.eventbus.EventBus
import utils.IdentityType
import utils.LogEvent
import javax.inject.Inject

/**
 * This class represents certification authority, which can create certificates and validate them
 * RSA system is used for signature
 */
class CertificationAuthorityImpl @Inject constructor(
        private val keyGenerator: KeyGenerator,
        private val rsa: RSA
) : CertificationAuthority {

    //RSA key pair of certification authority
    private val caKeyPair: KeyPair = keyGenerator.generateKeyPair()
    private var lastSerialNumber = 0
    private val activeCertificates = HashMap<Int, Certificate>()
    private val revokedCertificates = HashMap<Int, Certificate>()


    override fun init() {
        //send messages to Certification authority log via eventBus
        EventBus.getDefault().post(LogEvent(IdentityType.CERTIFICATION_AUTHORITY, "Creating certification authority..."))
        EventBus.getDefault().post(LogEvent(IdentityType.CERTIFICATION_AUTHORITY, "Certification authority key pair created"))
    }

    /**
     * Function for creating a certificate together with private key
     */
    override fun issueCertificate(userCredentials: Subject): Pair<Certificate, PrivateKey> {
        //notify UI
        EventBus.getDefault().post(LogEvent(IdentityType.CERTIFICATION_AUTHORITY, "User: ${userCredentials.CN} is asking for Certificate..."))
        //generate keypair
        val keyPair = keyGenerator.generateKeyPair()
        //create certificate object
        val certificate = Certificate(
                serialNumber = ++lastSerialNumber,
                issuer = Subject.caAuthority(),
                subject = userCredentials,
                publicKeyInfo = PublicKeyInfo(publicKey = keyPair.publicKey)
        )
        //get certificate hash
        val certificateHash = certificate.getSha1Hash()
        //sign certificate
        certificate.certificateSignature = rsa.encrypt(certificateHash, caKeyPair.privateKey)
        //notify UI
        EventBus.getDefault().post(LogEvent(IdentityType.CERTIFICATION_AUTHORITY, "Certificate created and signed by CA"))
        //add certificate to active certificates
        activeCertificates[certificate.serialNumber] = certificate
        return Pair(certificate, keyPair.privateKey)
    }

    /**
     * When certificate is revoked, it is deleted from list (hashmap) of active certificates and added to list of revoked certificates
     */
    override fun revokeCertificate(certificateSerialNumber: Int) {
        val certificate = activeCertificates.getOrDefault(certificateSerialNumber, null) ?: return
        revokedCertificates[certificateSerialNumber] = certificate
        activeCertificates.remove(certificateSerialNumber)
        //notify UI
        EventBus.getDefault().post(LogEvent(IdentityType.CERTIFICATION_AUTHORITY, "Certificate with serial number $certificateSerialNumber was revoked."))
    }

    /**
     * Checks, whether certificate was revoked and if whether it is expired
     */
    override fun isCertificateValid(serialNumber: Int): Boolean {
        val certificate = activeCertificates[serialNumber] ?: return false
        val now = System.currentTimeMillis()
        if (now > certificate.validity.notAfter || now < certificate.validity.notBefore) {
            return false
        }
        if (revokedCertificates.containsKey(serialNumber)) return false
        return true
    }

    /**
     * Get public key of certification authority
     */
    override fun getPublicKey(): PublicKey {
        return caKeyPair.publicKey
    }

    override fun getActiveCertificates(): ArrayList<Int> {
        val activeCertificatesSN = ArrayList<Int>()
        activeCertificates.keys.forEach {
            activeCertificatesSN.add(it)
        }
        return activeCertificatesSN
    }

    override fun getRevokeCertificates(): ArrayList<Int> {
        val revokedCertificatesSN = ArrayList<Int>()
        revokedCertificates.keys.forEach {
            revokedCertificatesSN.add(it)
        }
        return revokedCertificatesSN
    }
}