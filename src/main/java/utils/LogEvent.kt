package utils

/**
 * Log event is dataObject, which describes message to be displayed to application user. IdentityType defines in which
 * log should specific message appear
 */
data class LogEvent(val identityType: IdentityType, val message: String)