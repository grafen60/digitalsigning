package utils

/**
 * Works as simple id for major components in application
 */
enum class IdentityType {
    USER_1, USER_2, CERTIFICATION_AUTHORITY
}